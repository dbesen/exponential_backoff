#!/bin/bash

# todo: argument validation (at least correct # of args)
# if 2nd arg is not provided, skip first step

cd "$( dirname "${BASH_SOURCE[0]}" )"

./exponential_backoff.sh "! host $1"
