#!/bin/bash

SLEEP_TIME=1

while true; do
    eval "$@"
    RETCODE=$?
    if [ $RETCODE -eq 0 ]; then
        exit 0
    fi
    date | tr -d '\n'
    echo "; sleeping $SLEEP_TIME"
    sleep $SLEEP_TIME
    SLEEP_TIME=`echo "scale=10;$SLEEP_TIME*2" | bc`
done
