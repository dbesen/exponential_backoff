#!/bin/bash

# todo: argument validation (at least correct # of args)

cd "$( dirname "${BASH_SOURCE[0]}" )"

./exponential_backoff.sh "! ping -c 1 -w 1 $1"
